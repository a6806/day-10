import argparse
from typing import TextIO


class NavigationSyntaxError(RuntimeError):
    def __init__(self, message, symbol):
        self.message = message
        self.symbol = symbol

    def __str__(self):
        return self.message


class Chunk:
    def __init__(self, symbol, parent):
        self.symbol = symbol
        self.parent = parent
        self.children = []

    def add_child(self, chunk):
        self.children.append(chunk)

    def get_closing_symbol(self):
        if self.symbol == '(':
            return ')'
        if self.symbol == '[':
            return ']'
        if self.symbol == '{':
            return '}'
        if self.symbol == '<':
            return '>'

    def is_closing_symbol(self, symbol):
        return symbol == self.get_closing_symbol()

    def process_symbol(self, symbol):
        if self.is_closing_symbol(symbol):
            self.parent.add_child(self)
            return self.parent
        if symbol in ['(', '[', '{', '<']:
            return Chunk(symbol, self)
        raise NavigationSyntaxError(f'Expected {self.get_closing_symbol()}, but found {symbol} instead', symbol)


class AST:
    def __init__(self):
        self.root = Chunk('x', None)
        self.current_chunk = self.root

    def process_symbol(self, symbol):
        self.current_chunk = self.current_chunk.process_symbol(symbol)

    def complete(self):
        completion = []
        while self.current_chunk is not self.root:
            symbol = self.current_chunk.get_closing_symbol()
            completion.append(symbol)
            self.current_chunk = self.current_chunk.process_symbol(symbol)
        return completion


SCORE_MAP = {
    ')': 3,
    ']': 57,
    '}': 1197,
    '>': 25137,
}

SCORE_MAP_2 = {
    ')': 1,
    ']': 2,
    '}': 3,
    '>': 4,
}


def part_1(input_file: TextIO):
    lines = [x for x in input_file.read().split('\n')]
    score = 0
    for line_index, line in enumerate(lines):
        ast = AST()
        for character in line:
            try:
                ast.process_symbol(character)
            except NavigationSyntaxError as e:
                print(f'{line_index + 1}: {e}')
                score += SCORE_MAP[e.symbol]
                break
    return score


def part_2(input_file: TextIO):
    lines = [x for x in input_file.read().split('\n')]
    scores = []
    for line_index, line in enumerate(lines):
        ast = AST()
        error = False
        for character in line:
            try:
                ast.process_symbol(character)
            except NavigationSyntaxError:
                error = True
                break
        if error:
            continue

        completion = ast.complete()
        score = 0
        for character in completion:
            score = score * 5 + SCORE_MAP_2[character]
        scores.append(score)

    scores = sorted(scores)
    middle = scores[int(len(scores) / 2)]
    return middle


def main():
    parser = argparse.ArgumentParser(description='Advent of Code Day 10')
    part_group = parser.add_mutually_exclusive_group(required=True)
    part_group.add_argument('--part-1', action='store_true', help='Run Part 1')
    part_group.add_argument('--part-2', action='store_true', help='Run Part 2')
    parser.add_argument('--example', action='store_true', help='Run part with example data')
    args = parser.parse_args()

    part_number = '1' if args.part_1 else '2'
    function = part_1 if args.part_1 else part_2
    example = '_example' if args.example else ''
    input_file_path = f'input/part_{part_number}{example}.txt'

    with open(input_file_path, 'r') as input_file:
        print(function(input_file))


if __name__ == '__main__':
    main()
